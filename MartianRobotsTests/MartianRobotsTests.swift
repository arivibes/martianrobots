//
//  MartianRobotsTests.swift
//  MartianRobotsTests
//
//  Created by Ariel Elkin on 15/12/2018.
//  Copyright © 2018 ariel. All rights reserved.
//

import XCTest

class MartianRobotsTests: XCTestCase {

    func testInitWorldWithInvalidInput() {
        XCTAssertThrowsError(try World.init("8jasdf90jasjdfa")) { error in
            if let error = error as? MartianError {
                XCTAssert(error == MartianError.WorldInitialisedWithInvalidParameters)
            } else {
                XCTFail()
            }
        }
    }

    func testInitRobotWithInvalidInput() {
        XCTAssertThrowsError(try Robot("asdf9a")) { error in
            if let error = error as? MartianError {
                XCTAssert(error == MartianError.RobotInitialisedWithInvalidParameters)
            }
            else {
                XCTFail()
            }
        }
    }

    func testRobotMovesInPlace() {

        let robotInput = "1 1 E RFRFRFRF"
        guard let robot = try? Robot(robotInput), let world = try? World("5 3") else {
            return XCTFail()
        }

        let (finalPosition, fell, newWorld) = world.process(robot: robot)

        XCTAssert(finalPosition.point.x == 1)
        XCTAssert(finalPosition.point.y == 1)
        XCTAssertEqual(finalPosition.orientation, Orientation.E)
        XCTAssert(fell == false)
        XCTAssert(newWorld.scents.count == 0)
    }

    func testRobotCanFallOff() {

        let robotInput = "3 2 N FRRFLLFFRRFLL"
        guard let robot = try? Robot(robotInput), let world = try? World("5 3") else {
            return XCTFail()
        }

        let (finalPosition, fell, newWorld) = world.process(robot: robot)

        XCTAssert(finalPosition.point.x == 3)
        XCTAssert(finalPosition.point.y == 3)
        XCTAssertEqual(finalPosition.orientation, Orientation.N)
        XCTAssert(fell == true)
        guard let scent = newWorld.scents.first else {
            return XCTFail()
        }
        XCTAssert(scent.x == 3)
        XCTAssert(scent.y == 3)
    }

    func testRobotRespondsToScent() {

        let firstRobotInput = "3 2 N FRRFLLFFRRFLL"
        let secondRobotInput = "0 3 W LLFFFLFLFL"
        guard let firstRobot = try? Robot(firstRobotInput), let secondRobot = try? Robot(secondRobotInput), let firstWorld = try? World("5 3") else {
            return XCTFail()
        }

        let (firstRobotFinalPosition, firstRobotFell, secondWorld) = firstWorld.process(robot: firstRobot)

        XCTAssert(firstRobotFinalPosition.point.x == 3)
        XCTAssert(firstRobotFinalPosition.point.y == 3)
        XCTAssert(firstRobotFinalPosition.orientation == Orientation.N)
        XCTAssert(firstRobotFell == true)

        guard let secondWorldScents = secondWorld.scents.first else {
            return XCTFail()
        }
        XCTAssert(secondWorldScents.x == 3)
        XCTAssert(secondWorldScents.y == 3)


        let (secondRobotFinalPosition, secondRobotFell, thirdWorld) = secondWorld.process(robot: secondRobot)

        XCTAssert(secondRobotFinalPosition.point.x == 2)
        XCTAssert(secondRobotFinalPosition.point.y == 3)
        XCTAssert(secondRobotFinalPosition.orientation == Orientation.S)
        XCTAssert(secondRobotFell == false)
        guard let thirdWorldScents = thirdWorld.scents.first else {
            return XCTFail()
        }
        XCTAssert(thirdWorldScents.x == 3)
        XCTAssert(thirdWorldScents.y == 3)
    }
}
