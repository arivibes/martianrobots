//
//  main.swift
//  MartianRobots
//
//  Created by Ariel Elkin on 15/12/2018.
//  Copyright © 2018 ariel. All rights reserved.
//

import Foundation

print("Hello, Mars!")

struct Point {
    let x: Int
    let y: Int
}

extension Point: CustomStringConvertible {
    var description: String {
        return "\(self.x) \(self.y)"
    }
}

struct Position {
    let point: Point
    let orientation: Orientation
}

extension Position: CustomStringConvertible {
    var description: String {
        return "\(point) \(orientation)"
    }
}

enum Instruction {
    case F, L, R
    init?(_ input: Character) {
        switch input {
        case "F":
            self = .F
        case "L":
            self = .L
        case "R":
            self = .R
        default:
            return nil
        }
    }
}

enum Orientation {
    case N, S, E, W
    init?(_ input: String) {
        switch input {
        case "N":
            self = .N
        case "E":
            self = .E
        case "S":
            self = .S
        case "W":
            self = .W
        default:
            return nil
        }
    }

    func rotate(_ instruction: Instruction) -> Orientation? {
        switch instruction {
        case .L:
            switch self {
            case .N:
                return .W
            case .E:
                return .N
            case .S:
                return .E
            case .W:
                return .S
            }
        case .R:
            switch self {
            case .N:
                return .E
            case .E:
                return .S
            case .S:
                return .W
            case .W:
                return .N
            }
        case .F:
            return nil
        }
    }
}


enum MartianError: Error {
    case WorldInitialisedWithInvalidParameters
    case RobotInitialisedWithInvalidParameters
}

struct World {
    let width: Int
    let height: Int

    let scents: [Point]

    init(_ input: String) throws {
        let dimensions = input.components(separatedBy: " ")

        guard dimensions.count == 2 else {
            throw MartianError.WorldInitialisedWithInvalidParameters
        }

        if let width = Int(dimensions.first!), let height = Int(dimensions[1]) {
            self.width = width
            self.height = height
        }
        else {
            throw MartianError.WorldInitialisedWithInvalidParameters
        }
        self.scents = [Point]()
    }

    init(width: Int, height: Int, scents: [Point]) {
        self.width = width
        self.height = height
        self.scents = scents
    }

    func process(robot: Robot) -> (finalPosition: Position, fell: Bool, newWorld: World) {
        let (finalPosition, fellOff) = robot.process(world: self)
        if fellOff {
            var newScents = self.scents
            newScents.append(finalPosition.point)
            return (finalPosition, fellOff, World(width: self.width, height: self.height, scents: newScents))
        }
        return (finalPosition, fellOff, World.init(width: self.width, height: self.height, scents: scents))
    }
}

struct Robot {

    let initialPosition: Position
    let instructions: String

    init(_ input: String) throws {
        let positionAndInstructions = input.components(separatedBy: " ")

        guard positionAndInstructions.count == 4 else {
            throw MartianError.RobotInitialisedWithInvalidParameters
        }

        if let positionX = Int(positionAndInstructions.first!),
            let positionY = Int(positionAndInstructions[1]),
            let orientation = Orientation(positionAndInstructions[2]) {
            self.initialPosition = Position(point: Point(x: positionX, y: positionY), orientation: orientation)
        } else {
            throw MartianError.RobotInitialisedWithInvalidParameters
        }
        self.instructions = positionAndInstructions[3]
    }

    fileprivate func process(world: World) -> (finalPosition: Position, fell: Bool) {
        var currentPosition = initialPosition

        for instruction in instructions {
            guard let instruction = Instruction(instruction) else {
                break
            }

            if let newDirection = currentPosition.orientation.rotate(instruction) {
                currentPosition = Position(point: currentPosition.point, orientation: newDirection)
            }
            else {

                let newPoint: Point
                switch currentPosition.orientation {
                case .N:
                    newPoint = Point(x: currentPosition.point.x, y: currentPosition.point.y + 1)
                case .S:
                    newPoint = Point(x: currentPosition.point.x, y: currentPosition.point.y - 1)
                case .E:
                    newPoint = Point(x: currentPosition.point.x + 1, y: currentPosition.point.y)
                case .W:
                    newPoint = Point(x: currentPosition.point.x - 1, y: currentPosition.point.y)
                }

                if newPoint.x > world.width || newPoint.x < 0 || newPoint.y > world.height || newPoint.y < 0 {
                    if !world.scents.contains { point in return (point.x == currentPosition.point.x && point.y == currentPosition.point.y) } {
                        currentPosition = Position(point: currentPosition.point, orientation: currentPosition.orientation)
                        return (currentPosition, true)
                    }
                } else {
                    currentPosition = Position(point: newPoint, orientation: currentPosition.orientation)
                }
            }
        }
        return (currentPosition, false)
    }
}

print("Creating World. Please specify width and height. E.g. '5 3'")

var world: World?
var firstRobotLine: String?

while let input = readLine() {

    if let currentWorld = world {
        if let line = firstRobotLine {
            do {
                let robotInput = line + " " + input
                let robot = try Robot(robotInput)
                print("Added a Robot: \(String(reflecting: robot))")

                let (finalPosition, fell, newWorld) = currentWorld.process(robot: robot)
                print("Robot's final position:")
                print(finalPosition, fell ? "LOST" : "")
                print("World's scents: \(newWorld.scents)")

                world = newWorld
                firstRobotLine = nil
            }
            catch {
                print("Failed to create robot: \(error)")
                firstRobotLine = nil
            }
            print("Creating Robot. Please specify initial position. E.g. '1 1 E'")
        }
        else {
            print("Please specify robot's instructions. E.g. 'RFRFRFRF'")
            firstRobotLine = input
        }
    }
    else {
        do {
            world = try World(input)
            print("World created: \(String(reflecting: world!))")
            print("Creating Robot. Please specify initial position. E.g. '1 1 E'")
        } catch {
            print("Failed to create world: \(error)")
            print("Creating World. Please specify width and height. E.g. '5 3'")
        }
    }
}
